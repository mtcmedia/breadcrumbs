This package offers a simple implementation of breacrumbs using Laravel stack

#Installation
Require via composer

composer require mtcmedia/breadcrumbs

#Usage
```
use Mtc\Breadcrumbs\Facades\Breadcrumbs;

// Add a crumb
Breadcrumbs::addCrumb('Settings', route('settings.index'));

// Display (in template)
@foreach(app('breadcrumbs')->toArray() as $breadcrumb)
    <li class="breadcrumb-item">
        <a href="{{ $breadcrumb['route'] }}">
            {{ $breadcrumb['title'] }}
        </a>
    </li>
@endforeach
```

#Contributing
Please see [Contributing](CONTRIBUTING.md) for details.