<?php

namespace Mtc\Breadcrumbs;

use Illuminate\Contracts\Support\Arrayable;

class Breadcrumbs implements Arrayable
{
    protected array $breadcrumbs = [];

    public function addCrumb(string $title, string $route): void
    {
        $this->breadcrumbs[] = [
            'title' => $title,
            'route' => $route
        ];
    }

    public function toArray(): array
    {
        return $this->breadcrumbs;
    }
}
