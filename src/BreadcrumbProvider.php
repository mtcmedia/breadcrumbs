<?php

namespace Mtc\Breadcrumbs;

use Illuminate\Support\ServiceProvider;
use Mtc\Breadcrumbs\Breadcrumbs;

class BreadcrumbProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton('breadcrumbs', function () {
            return new Breadcrumbs();
        });
    }
}
