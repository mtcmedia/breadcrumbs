<?php

namespace Mtc\Breadcrumbs\Facades;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Facade;
use Mtc\Admin\Models\Menu;

/**
 * @method static void addCrumb($title, $route)
 * @method static array toArray()
 *
 */
class Breadcrumbs extends Facade
{
    /**
     * Define the facade accessor
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'breadcrumbs';
    }
}
