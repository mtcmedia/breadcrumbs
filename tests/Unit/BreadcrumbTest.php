<?php

namespace Tests\Unit;

use Mtc\Breadcrumbs\Breadcrumbs;
use Tests\TestCase;

class BreadcrumbTest extends TestCase
{
    public function test_breadcrumb_has_title()
    {
        $crumbs = new Breadcrumbs();
        $crumbs->addCrumb('title', 'route');
        $result = $crumbs->toArray();
        self::assertEquals('title', $result[0]['title']);
    }

    public function test_breadcrumb_has_route()
    {
        $crumbs = new Breadcrumbs();
        $crumbs->addCrumb('title', 'route');
        $result = $crumbs->toArray();
        self::assertEquals('route', $result[0]['route']);
    }

    public function test_able_to_retrieve_crumb()
    {
        $crumbs = new Breadcrumbs();
        $crumbs->addCrumb('test', 'test');
        $result = $crumbs->toArray();
        self::assertIsArray($result);
    }

    public function test_adding_multiple_crumbs_returns_all_crumbs()
    {
        $crumbs = new Breadcrumbs();
        $crumbs->addCrumb('test', 'test');
        self::assertEquals(1, count($crumbs->toArray()));

        $crumbs->addCrumb('test2', 'test2');
        self::assertEquals(2, count($crumbs->toArray()));

        $crumbs->addCrumb('test3', 'test3');
        self::assertEquals(3, count($crumbs->toArray()));
    }
}

